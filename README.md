### What is this repository for? ###

* This is a PoC on how to convert a normal line chart created with POI into a bar chart (at the time of writing POI wasnt able to create bar charts)
* the latest version does this with the help of docx4j 

### How do I get set up? ###

* see test class: TransformLineChartWithDocx4jTest

