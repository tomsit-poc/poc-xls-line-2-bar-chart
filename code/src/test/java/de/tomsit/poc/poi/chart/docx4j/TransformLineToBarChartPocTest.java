package de.tomsit.poc.poi.chart.docx4j;

import static org.junit.Assert.assertTrue;

import java.io.File;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author tmenzel
 * @since 21.04.2015
 */
public class TransformLineToBarChartPocTest {

	/** OUT_ROOT */
	private static final String OUT_ROOT_PATH = "target/"
		+ TransformLineToBarChartPocTest.class.getSimpleName();

	/** OUT_ROOT */
	private static final File OUT_ROOT = new File(OUT_ROOT_PATH);

	/** */
	@Before
	public void setUp() throws Exception {

		final File path = new File(this.getClass().getResource("line-chart.xlsx").toURI());

		final ZipFile zipFile = new ZipFile(path);
		if (OUT_ROOT.exists()) {
			FileUtils.deleteDirectory(OUT_ROOT);
		}
		zipFile.extractAll(OUT_ROOT_PATH);

	}

	/**
	 * creating the zip works
	 */
	@Test
	@Ignore
	public void testZipUp() throws Exception {
		createZip();
	}

	/**
	 * @throws ZipException
	 */
	private void createZip() throws ZipException {
		final ZipParameters params = new ZipParameters();
		params.setIncludeRootFolder(false);
		final File zipFileObj = new File(OUT_ROOT_PATH + ".xlsx");
		if (zipFileObj.exists()) {
			assertTrue("cannot delete zip: " + zipFileObj, zipFileObj.delete());
		}
		final ZipFile zipFile = new ZipFile(OUT_ROOT + ".xlsx");
		zipFile.createZipFileFromFolder(OUT_ROOT, params, false, 0);
	}

	/**
	 * this unzips an simple line chart XLSX and mods the chart.xml such that it
	 * becomes a bar chart - by replacing the strings :oO (i know that's bad
	 * style... <br/>
	 * process was such:
	 * <ol>
	 * <li>the line chart in src XLSX was converted to a bar chart in excel
	 * itself</li>
	 * <li>this file the saved and unzip and code formatted by eclipse, so we
	 * can compare it properly</li>
	 * <li>XLSX produced by this test then was diff'ed against the barchar
	 * refetenece from the step before</li>
	 * </ol>
	 */
	@Test
	public void testTrandformWithStringReplace() throws Exception {
		final File chartFile = new File(OUT_ROOT, "xl/charts/chart1.xml");
		String cont = FileUtils.readFileToString(chartFile, "UTF-8");
		cont = StringUtils.replace(cont, "<c:lineChart>", "<c:barChart>"
			+ "<c:barDir val=\"col\" />" + "<c:grouping val=\"stacked\" />");
		cont = StringUtils.replace(cont, "</c:lineChart>", "        <c:dLbls>\r\n"
			+ "          <c:showLegendKey val=\"0\" />\r\n"
			+ "          <c:showVal val=\"0\" />\r\n" + "          <c:showCatName val=\"0\" />\r\n"
			+ "          <c:showSerName val=\"0\" />\r\n"
			+ "          <c:showPercent val=\"0\" />\r\n"
			+ "          <c:showBubbleSize val=\"0\" />\r\n" + "        </c:dLbls>"
			+ "<c:gapWidth val='150' /><c:overlap val='100' />".replace('\'', '"')
			+ "</c:barChart>");

		/*
		 * NOTE: these transforms are not a must - at least XLS doesnt
		 * complain... | tmenzel @ 21.04.2015
		 */
		// cont = StringUtils.replace(cont,
		// "<c:marker><c:symbol val=\"none\"/></c:marker>",
		// "<c:invertIfNegative val=\"0\" />");
		// cont = StringUtils.replace(cont, "<c:numCache>", "<c:numCache>"
		// + "<c:formatCode>General</c:formatCode>");

		FileUtils.writeStringToFile(chartFile, cont, "UTF-8");
		createZip();

	}

}
