package de.tomsit.poc.poi.chart.docx4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.io.SaveToZipFile;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.samples.PartsList;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xlsx4j.sml.CTDrawing;
import org.xlsx4j.sml.Worksheet;

/**
 * @author tmenzel
 * @since 21.04.2015
 */
public class Docx4jChartPocTest {

	private WorkbookPart wbp;

	protected SpreadsheetMLPackage root;

	/** Logger. */
	private final static Logger log = Logger.getLogger(Docx4jChartPocTest.class);

	/** */
	@Before
	public void setUp() throws Exception {

		final InputStream inp = this.getClass().getResourceAsStream("../workbook.xlsx");
		root = (SpreadsheetMLPackage) SpreadsheetMLPackage.load(inp);
		wbp = root.getWorkbookPart();
	}

	@Test
	public void testPartList() throws Exception {
		final String path = new File(this.getClass().getResource("../workbook.xlsx").toURI())
			.getPath();
		PartsList.main(new String[] { path });
	}

	@Test
	public void testRels() throws Exception {
		final RelationshipsPart rp = wbp.getRelationshipsPart();
		Docx4TestUtils.traverseRels(root, rp);

	}

	@Test
	public void testRoundTripp() throws Exception {
		final SaveToZipFile saver = new SaveToZipFile(root);
		saver.save("workbook-Docx4j.xlsx");
	}

	/**
	 * how to copy/clone a page containing a chart
	 * 
	 * @result couldnt figure out what all needs to be done, for a valid XLSX to
	 *         come out...
	 */
	@Test
	@Ignore
	public void testCopyPageWithChart_() throws Exception {

		final WorksheetPart sheet1 = wbp.getWorksheet(0);
		final WorksheetPart sheet2 = root.createWorksheetPart(new PartName(
			"/xl/worksheets/sheet2.xml"), "sheet 2 / label", 0);
		final Worksheet contents = sheet1.getContents();
		sheet2.setContents(deepClone(contents, Worksheet.class));

		wbp.addTargetPart(sheet2);

		final CTDrawing drawing1 = sheet1.getContents().getDrawing();
		final CTDrawing drawing2 = deepClone(drawing1, CTDrawing.class);
		drawing2.setId("drawing2");
		drawing2.setParent(sheet2);
		sheet2.getContents().setDrawing(drawing2);

		// sheet2.getRelationshipsPart().addTargetPart(part);

		assertNotEquals(drawing1.getId(), sheet2.getContents().getDrawing().getId());

		Docx4TestUtils.traverseRels(root, sheet1.getRelationshipsPart());
		Docx4TestUtils.traverseRels(root, sheet2.getRelationshipsPart());

		// root.save(new File("docx4j-clone-sheet.xlsx"));

	}

	/**
	 * Howto get to the CTChart on a worksheet if there are multiple sheets
	 * present containing charts.
	 */
	@Test
	public void howtoGetChartOnAWorksheet() throws Exception {

		final int sheetIndex = 0;

		final WorksheetPart wsPart = wbp.getWorksheet(sheetIndex);

		final Set<Part> foundParts = findDescendendsByType(wsPart, new HashSet<Part>(),
			"application/vnd.openxmlformats-officedocument.drawingml.chart+xml");
		System.out.println(foundParts);

		assertEquals(1, foundParts.size());
		final Part chartPart = foundParts.iterator().next();
		assertEquals("application/vnd.openxmlformats-officedocument.drawingml.chart+xml",
			chartPart.getContentType());
		assertEquals(Chart.class, chartPart.getClass());

	}

	/**
	 * Find descendends by type.
	 *
	 * @param rootPart
	 *            either part itself or its {@link RelationshipsPart}
	 * @param foundParts
	 *            set to collect the matching Parts, in case these are ref'ed
	 *            from >1 sources it is a set
	 * @param type
	 *            the type to be matched
	 * @return same as the param foundParts
	 */
	public static Set<Part> findDescendendsByType(final Part rootPart, final Set<Part> foundParts,
		final String type) {

		final RelationshipsPart rp = rootPart instanceof RelationshipsPart ? (RelationshipsPart) rootPart
			: rootPart.getRelationshipsPart(false);
		if (rp == null) {
			return foundParts;
		}

		for (final Relationship r : rp.getRelationships().getRelationship()) {

			log.info("\nFor Relationship Id=" + r.getId() + " Source is "
				+ rp.getSourceP().getPartName() + ", Target is " + r.getTarget() + " type "
				+ r.getType() + "\n");

			if (StringUtils.equals("External", r.getTargetMode())) {
				// shouldnt happen in our case
				final String msg = "external resource " + r.getTarget() + " of type " + r.getType();
				log.warn(msg);
				continue;
			}

			final Part part = rp.getPart(r);

			if (type.equals(part.getContentType())) {
				foundParts.add(part);
			}

			// if (handled.get(part) != null) {
			// sb.append(" [additional reference] ");
			// continue;
			// }
			// handled.put(part, part);
			if (part.getRelationshipsPart(false) == null) {
				// sb.append(".. no rels" );
			}
			else {
				findDescendendsByType(part, foundParts, type);
			}

		}
		return foundParts;
	}

	/**
	 * @param drawing1
	 * @param class1
	 *            TODO
	 * @return
	 * @throws JAXBException
	 */
	public <T> T deepClone(final T drawing1, final Class<?>... class1) throws JAXBException {
		return XmlUtils.deepCopy(drawing1, JAXBContext.newInstance(class1));
	}
}
