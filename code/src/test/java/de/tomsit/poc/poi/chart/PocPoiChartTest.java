package de.tomsit.poc.poi.chart;

import static java.lang.String.format;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.charts.AxisCrosses;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.ss.usermodel.charts.ChartAxis;
import org.apache.poi.ss.usermodel.charts.ChartDataSource;
import org.apache.poi.ss.usermodel.charts.DataSources;
import org.apache.poi.ss.usermodel.charts.LegendPosition;
import org.apache.poi.ss.usermodel.charts.LineChartData;
import org.apache.poi.ss.usermodel.charts.ValueAxis;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;
import org.junit.Before;
import org.junit.Test;

/**
 * @author tmenzel
 * @since 21.04.2015
 */
public class PocPoiChartTest {

	private XSSFWorkbook wb;

	/** */
	@Before
	public void setUp() throws Exception {
		wb = loadWorkbook("workbook.xlsx");
	}

	/**
	 * Tests whether copying a sheet will also copy the chart on it.
	 * 
	 * @result invalid XLSX
	 * @see https://bz.apache.org/bugzilla/show_bug.cgi?id=54470
	 */
	@Test
	public void testCopyPageWithChart_cloneShet_fails() throws Exception {
		final XSSFSheet sheet = wb.getSheetAt(0);
		final XSSFSheet clone = wb.cloneSheet(0);

		// Write the output to a file
		writeXslx("clone-sheet.xlsx");

	}

	/**
	 * @result renaming the sheet will result in an invalid XLS since the ref's
	 *         in the chart to the data series are not considered by poi and
	 *         hence not "renamed"
	 */
	@Test
	public void testRenameSheet() throws Exception {
		wb.setSheetName(0, "my name");

		writeXslx("rename-sheet.xlsx");
	}

	/**
	 * this creates a line chart (the code has been taken from a website) with
	 * POI and then converts it with an XLS macro when the respective page is
	 * opened.
	 */
	@Test
	public void testConvertLineCharToBarChartViaMacro() throws Exception {

		wb = loadWorkbook("workbook.xlsm");

		/*
		 * Create a worksheet object for the line chart. This worksheet will
		 * contain the chart
		 */
		// wb = new XSSFWorkbook();
		addSheetWithLineChart(wb, 5, "chart");

		writeXslx("line-chart-macro.xlsm");

	}

	@Test
	public void createWorkbook() throws Exception {
		wb = new XSSFWorkbook();
		wb.createSheet("pre");

		for (int i = 1; i < 6; i++) {
			addSheetWithLineChart(wb, 53, "chart" + i);
		}

		writeXslx("line-chart-1+4+2.xlsx");
	}
	/**
	 * @param name TODO
	 */
	public static void addSheetWithLineChart(final XSSFWorkbook wb, final int maxCols, final String name) {
		final XSSFSheet my_worksheet = wb.createSheet(name);

		/* Let us now create some test data for the chart */
		/*
		 * Later we can see how to get this test data from a CSV File or SQL
		 * Table
		 */
		/* We use a 4 Row chart input with max columns each */
		for (int rowIndex = 0; rowIndex < 5; rowIndex++) {
			/* Add a row that contains the chart data */
			final XSSFRow my_row = my_worksheet.createRow((short) rowIndex);
			for (int colIndex = 0; colIndex < maxCols; colIndex++) {
				/* Define column values for the row that is created */
				final XSSFCell cell = my_row.createCell((short) colIndex);
				cell.setCellValue(colIndex * (rowIndex + 1));
			}
		}
		/*
		 * At the end of this step, we have a worksheet with test data, that we
		 * want to write into a chart
		 */
		/* Create a drawing canvas on the worksheet */
		final XSSFDrawing xlsx_drawing = my_worksheet.createDrawingPatriarch();
		/* Define anchor points in the worksheet to position the chart */
		final XSSFClientAnchor anchor = xlsx_drawing.createAnchor(0, 0, 0, 0, 0, 5, maxCols, 25);
		/* Create the chart object based on the anchor point */
		final XSSFChart my_line_chart = xlsx_drawing.createChart(anchor);
		/* Define legends for the line chart and set the position of the legend */
		final XSSFChartLegend legend = my_line_chart.getOrCreateLegend();
		legend.setPosition(LegendPosition.BOTTOM);
		/* Create data for the chart */
		final LineChartData data = my_line_chart.getChartDataFactory().createLineChartData();
		/* Define chart AXIS */
		final ChartAxis bottomAxis = my_line_chart.getChartAxisFactory().createCategoryAxis(
			AxisPosition.TOP);
		final ValueAxis leftAxis = my_line_chart.getChartAxisFactory().createValueAxis(
			AxisPosition.LEFT);
		leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
		/* Define Data sources for the chart */
		/* Set the right cell range that contain values for the chart */
		/* Pass the worksheet and cell range address as inputs */
		/*
		 * Cell Range Address is defined as First row, last row, first column,
		 * last column
		 */
		final ChartDataSource<Number> xs = DataSources.fromNumericCellRange(my_worksheet,
			new CellRangeAddress(0, 0, 0, maxCols));
		final ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(my_worksheet,
			new CellRangeAddress(1, 1, 0, maxCols));
		final ChartDataSource<Number> ys2 = DataSources.fromNumericCellRange(my_worksheet,
			new CellRangeAddress(2, 2, 0, maxCols));
		final ChartDataSource<Number> ys3 = DataSources.fromNumericCellRange(my_worksheet,
			new CellRangeAddress(3, 3, 0, maxCols));
		/* Add chart data sources as data to the chart */
		data.addSeries(xs, ys1);
		data.addSeries(xs, ys2);
		data.addSeries(xs, ys3);
		/* Plot the chart with the inputs from data and chart axis */
		my_line_chart.plot(data, new ChartAxis[] { bottomAxis, leftAxis });
		/* Finally define FileOutputStream and write chart information */
	}

	/**
	 * @param fileName
	 * @return
	 */
	private XSSFWorkbook loadWorkbook(final String fileName) throws IOException {
		final InputStream inp = this.getClass().getResourceAsStream(fileName);
		assertNotNull(inp);
		return new XSSFWorkbook(inp);
	}

	/**
	 */
	private void writeXslx(final String fileName) throws Exception {
		final String path = format("target/%s/%s", this.getClass().getSimpleName(), fileName);
		new File(path).getParentFile().mkdirs();
		final FileOutputStream fileOut = new FileOutputStream(path);
		wb.write(fileOut);
		fileOut.close();
	}
}
