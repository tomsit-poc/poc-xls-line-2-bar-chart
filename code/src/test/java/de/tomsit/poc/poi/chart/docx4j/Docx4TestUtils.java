/**
 * 
 */
package de.tomsit.poc.poi.chart.docx4j;

import org.apache.commons.lang.StringUtils;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.samples.PartsList;

/**
 * @author tmenzel
 * @since 21.04.2015
 */
public class Docx4TestUtils {



	/**
	 * @param spreadsheetMLPackage
	 *            TODO
	 * @param rp
	 */
	public static void traverseRels(final OpcPackage spreadsheetMLPackage, final RelationshipsPart rp) {
		System.out.println(StringUtils.repeat("#", 120));
		System.out.println("traversing: " + rp.getPartName());
		final StringBuilder sb = new StringBuilder();
		// final root.get
		PartsList.traverseRelationships(spreadsheetMLPackage, rp, sb, " ");

		System.out.println(sb.toString());
	}

}