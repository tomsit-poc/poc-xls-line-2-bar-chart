package de.tomsit.poc.poi.chart.docx4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.io.FileUtils;
import org.docx4j.dml.CTSRgbColor;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTSolidColorFillProperties;
import org.docx4j.dml.chart.CTBarChart;
import org.docx4j.dml.chart.CTBarDir;
import org.docx4j.dml.chart.CTBarGrouping;
import org.docx4j.dml.chart.CTBarSer;
import org.docx4j.dml.chart.CTBoolean;
import org.docx4j.dml.chart.CTChartSpace;
import org.docx4j.dml.chart.CTDLblPos;
import org.docx4j.dml.chart.CTDLbls;
import org.docx4j.dml.chart.CTGapAmount;
import org.docx4j.dml.chart.CTLineChart;
import org.docx4j.dml.chart.CTLineSer;
import org.docx4j.dml.chart.CTOverlap;
import org.docx4j.dml.chart.STBarDir;
import org.docx4j.dml.chart.STBarGrouping;
import org.docx4j.dml.chart.STDLblPos;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * this test is a successor to {@link TransformLineToBarChartPocTest} an tries
 * to accomplish the same thing (transform a line chart in an XLSX into a bar
 * chart) using JAXB classes from docx4j.
 * 
 * @author tmenzel
 * @since 21.04.2015
 */
public class TransformLineChartWithDocx4jTest {

	/** SRC_XLSX */
	private static final String SRC_XLSX = "line-chart.xlsx";

	/** OUT_ROOT */
	private static final String OUT_ROOT_PATH = "target/"
		+ TransformLineChartWithDocx4jTest.class.getSimpleName();

	/** OUT_ROOT */
	private static final File OUT_ROOT = new File(OUT_ROOT_PATH);

	protected SpreadsheetMLPackage root;

	/** */
	@Before
	public void setUp() throws Exception {
		if (OUT_ROOT.exists()) {
			FileUtils.deleteDirectory(OUT_ROOT);
		}

		final InputStream inp = this.getClass().getResourceAsStream(SRC_XLSX);
		root = (SpreadsheetMLPackage) SpreadsheetMLPackage.load(inp);

		root.getWorkbookPart();
	}

	/**
	 * creates the xlsx by zipping the files
	 */
	private void createXlsxFromFiles(final String fileName) throws ZipException {
		final ZipParameters params = new ZipParameters();
		params.setIncludeRootFolder(false);
		final File zipFileObj = new File(OUT_ROOT_PATH + ".xlsx");
		if (zipFileObj.exists()) {
			assertTrue("cannot delete zip: " + zipFileObj, zipFileObj.delete());
		}
		final ZipFile zipFile = new ZipFile(fileName);
		zipFile.createZipFileFromFolder(OUT_ROOT, params, false, 0);
	}

	/**
	 * just to see if that works -> it does
	 */
	@Test
	@Ignore
	public void testReadChartWithJaxbFromFile() throws Exception {

		final CTChartSpace chartSpace = unmarshalChartXmlFromFile("xl/charts/chart1.xml");

		System.out.println(chartSpace);
	}

	/**
	 * creates the transformed XLSX by
	 * <ol>
	 * <li>read the xlsx with docx4j</li>
	 * <li>get to the JAXB model via objeect navigation</li>
	 * <li>transform the chart via JAXB model</li>
	 * <li>save opc package</li>
	 * </ol>
	 * 
	 * @result quite a bit faster
	 */
	@Test
	public void testChangeToBarChartAndSaveViaOpcPackage() throws Exception {

		final Chart chartPart = (Chart) root.getParts().get(new PartName("/xl/charts/chart1.xml"));
		final List<Object> charts = chartPart.getContents().getChart().getPlotArea()
			.getAreaChartOrArea3DChartOrLineChart();
		final List<Object> objects = charts;
		assertEquals(1, charts.size());
		assertEquals(CTLineChart.class, charts.get(0).getClass());

		final CTLineChart lineChart = (CTLineChart) charts.get(0);
		final CTBarChart barChart = createBarChartScaffolding();

		transformToBarChart(lineChart, barChart);

		// replace the line with the barchart
		charts.set(0, barChart);

		root.save(new File(OUT_ROOT_PATH + "-via-opcpackage.xlsx"));
	}

	/**
	 * creates the transformed XLSX by
	 * <ol>
	 * <li>extract src xls to folder</li>
	 * <li>read the chart.xml file via JAXB</li>
	 * <li>transform the chart via JAXB model</li>
	 * <li>save chart1.xml via marshalling</li>
	 * <li>zip the whole folder</li>
	 * </ol>
	 */
	@Test
	public void testChangeToBarChartAndSaveViaFileSystem() throws Exception {
		//
		final CTChartSpace chartSpace = unmarshalChartXmlFromFile("xl/charts/chart1.xml");
		final List<Object> charts = chartSpace.getChart().getPlotArea()
			.getAreaChartOrArea3DChartOrLineChart();
		assertEquals(1, charts.size());
		assertEquals(CTLineChart.class, charts.get(0).getClass());

		final CTLineChart lineChart = (CTLineChart) charts.get(0);
		final CTBarChart barChart = createBarChartScaffolding();

		transformToBarChart(lineChart, barChart);

		// replace the line with the barchart
		charts.set(0, barChart);

		marshalChartXmlToFile(chartSpace);
		createXlsxFromFiles(OUT_ROOT + "-via-FileSystem.xlsx");

	}

	/**
	 * copies the lineChart stuff over to the bar chart + some additional
	 * settings
	 */
	private void transformToBarChart(final CTLineChart lineChart, final CTBarChart barChart)
		throws IllegalAccessException, InvocationTargetException {
		// axis
		barChart.getAxId().addAll(lineChart.getAxId());
		// copy the data series to the barchar -- we dont need to mod them
		for (final CTLineSer ser : lineChart.getSer()) {

			final CTBarSer barSer = new CTBarSer();
			BeanUtilsBean2.getInstance().copyProperties(barSer, ser);

			// set the color of a stack segment/series
			final CTShapeProperties shapeProps = new CTShapeProperties();
			final CTSolidColorFillProperties fill = new CTSolidColorFillProperties();
			final CTSRgbColor rgb = new CTSRgbColor();
			rgb.setVal(new byte[] { (byte) 0xC0, (byte) 0, 1 });
			fill.setSrgbClr(rgb);
			shapeProps.setSolidFill(fill);
			barSer.setSpPr(shapeProps);

			barSer.getCat().getNumRef().setNumCache(null);
			barSer.getVal().getNumRef().setNumCache(null);

			// CTDLbls serDatLabels;
			// serDatLabels.s
			// barSer.setDLbls( serDatLabels );

			// set to null so all series use the default
			barSer.setDLbls(null);

			barChart.getSer().add(barSer);
		}
	}

	/**
	 * create the bar chart scaffolding and sets the bar chart unique props
	 */
	private CTBarChart createBarChartScaffolding() {
		final CTBarChart barChart = new CTBarChart();
		final CTBarDir barDir = new CTBarDir();
		barDir.setVal(STBarDir.COL);
		barChart.setBarDir(barDir);
		final CTBarGrouping grouping = new CTBarGrouping();
		grouping.setVal(STBarGrouping.STACKED);
		barChart.setGrouping(grouping);

		/**
		 * <pre>
		 * 	<c:dLbls>
		 * 		<c:showLegendKey val="0" />
		 * 		<c:showVal val="0" />
		 * 		<c:showCatName val="0" />
		 * 		<c:showSerName val="0" />
		 * 		<c:showPercent val="0" />
		 * 		<c:showBubbleSize val="0" />
		 * 	</c:dLbls>
		 * </pre>
		 */
		final CTBoolean ctTrue = new CTBoolean();
		ctTrue.setVal(true);

		final CTBoolean ctFalse = new CTBoolean();
		ctFalse.setVal(false);

		final CTDLbls dataLabels = new CTDLbls();
		final CTDLblPos lblPos = new CTDLblPos();
		lblPos.setVal(STDLblPos.CTR);
		dataLabels.setDLblPos(lblPos);

		dataLabels.setShowLegendKey(ctFalse);
		dataLabels.setShowVal(ctTrue);
		dataLabels.setShowCatName(ctFalse);
		dataLabels.setShowSerName(ctFalse);
		// dataLabels.setShowPercent(ctTrue);
		// dataLabels.setShowBubbleSize(ctTrue);
		dataLabels.setShowLeaderLines(ctFalse);
		// these are the defaults for the data series
		barChart.setDLbls(dataLabels);

		/**
		 * <pre>
		 * 	<c:gapWidth val="150" />
		 * 	<c:overlap val="100" />
		 * </pre>
		 */
		final CTGapAmount gap = new CTGapAmount();
		gap.setVal(150);
		barChart.setGapWidth(gap);
		final CTOverlap overlap = new CTOverlap();
		overlap.setVal((byte) 100);
		barChart.setOverlap(overlap);
		return barChart;
	}

	/**
	 * also unzips all -- also needed for creating the xlsx from files
	 */
	@SuppressWarnings("unchecked")
	private CTChartSpace unmarshalChartXmlFromFile(final String chartFile) throws Exception {
		final File path = new File(this.getClass().getResource(SRC_XLSX).toURI());
		final ZipFile zipFile = new ZipFile(path);
		zipFile.extractAll(OUT_ROOT_PATH);

		final File file = new File(OUT_ROOT, chartFile);
		final JAXBContext jaxbContext = JAXBContext.newInstance(CTChartSpace.class);
		final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		final JAXBElement<CTChartSpace> unstreamed = (JAXBElement<CTChartSpace>) jaxbUnmarshaller
			.unmarshal(file);
		final CTChartSpace chartSpace = unstreamed.getValue();
		return chartSpace;
	}

	/**
	 */
	private void marshalChartXmlToFile(final CTChartSpace obj) throws JAXBException {
		final File file = new File(OUT_ROOT, "xl/charts/chart1.xml");
		final JAXBContext jaxbContext = JAXBContext.newInstance(CTChartSpace.class);
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.marshal(obj, file);
	}

	/**
	 * how to get the the {@link CTChartSpace} Obj from the package
	 */
	@Test
	public void testGetToCtChart() throws Exception {

		final Chart chartPart = (Chart) root.getParts().get(new PartName("/xl/charts/chart1.xml"));

		final List<Object> charts = chartPart.getContents().getChart().getPlotArea()
			.getAreaChartOrArea3DChartOrLineChart();
		final List<Object> objects = charts;
		assertEquals(1, charts.size());

		assertEquals(CTLineChart.class, charts.get(0).getClass());

	}

	/**
	 * this test is just to measure how long it takes to load the WB (junit view
	 * reports that per test) with docx4 and since this is done it the setup
	 * method this is a NOP
	 */
	@Test
	public void testLoadWorkbok() throws Exception {
	}

}
