package de.tomsit.poc.xls.chart;

import static org.junit.Assert.fail;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.junit.Before;
import org.junit.Test;

public class XlsChartTransformerTest {

	/** SRC_XLS */
	private static final String SRC_XLS = "line-chart-1+4+2";

	private XlsChartTransformer transformer;

	private List<DataSeriesConfig> dsConfigs;

	/** */
	@Before
	public void setUp() throws Exception {
		final InputStream is = this.getClass().getResourceAsStream(SRC_XLS + ".xlsx");
		transformer = new XlsChartTransformer(is);
		is.close();
		
		
		dsConfigs = new ArrayList<DataSeriesConfig>();
		dsConfigs.add(new DataSeriesConfig(Color.red));
		dsConfigs.add(new DataSeriesConfig(Color.green));
		dsConfigs.add(new DataSeriesConfig(Color.black));

	}

	@Test
	public void testTransform_Error_NoChart() throws Exception {
		try {
			transformer.transformToBarChart(0, dsConfigs);
		}
		catch (final Exception e) {
			if (!e.getMessage().contains("no chart")) {
				fail("expected msg wrong:" + e.getMessage());
			}
		}
	}

	@Test
	public void testTransform_Error_MultipleCharts() throws Exception {
		try {
			transformer.transformToBarChart(5, dsConfigs);
		}
		catch (final Exception e) {
			if (!e.getMessage().contains("more than one chart")) {
				fail("expected msg wrong:" + e.getMessage());
			}
		}
	}

	@Test
	public void testTransform_S1_AllColors() throws Exception {
		transformer.transformToBarChart(1, dsConfigs);
		saveXls("-s1-all-colors.xlsx");
	}

	@Test
	public void testTransform_S1_NoColors() throws Exception {
		dsConfigs.clear();
		transformer.transformToBarChart(1, dsConfigs);
		saveXls("-s1-no-colors.xlsx");
	}

	@Test
	public void testTransform_S1_LineColor_whiteOnBlack() throws Exception {
		dsConfigs.get(2).labelColor = Color.white;
		transformer.transformToBarChart(1, dsConfigs);
		saveXls("-s1-line-color-white-on-black.xlsx");
	}

	@Test
	public void testTransform_S1_2Colors() throws Exception {
		dsConfigs.remove(2);
		transformer.transformToBarChart(1, dsConfigs);
		saveXls("-s1-2-colors.xlsx");
	}

	@Test
	public void testTransform_S1234() throws Exception {
		for (int i = 1; i < 5; i++) {
			transformer.transformToBarChart(i, dsConfigs);
		}
		saveXls("-s1234-4-colors.xlsx");
	}

	/**
	 * @param suffix
	 * @throws Docx4JException
	 * @throws FileNotFoundException
	 */
	private void saveXls(final String suffix) throws Exception {
		final FileOutputStream outputStream = new FileOutputStream("target/"
			+ this.getClass().getSimpleName() + "-" + suffix);
		transformer.save(outputStream);

		outputStream.close();
	}

}
