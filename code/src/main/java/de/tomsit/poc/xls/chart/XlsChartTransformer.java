/**
 * 
 */
package de.tomsit.poc.xls.chart;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.docx4j.dml.CTSRgbColor;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTSolidColorFillProperties;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTextCharacterProperties;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.CTTextParagraphProperties;
import org.docx4j.dml.chart.CTBarChart;
import org.docx4j.dml.chart.CTBarDir;
import org.docx4j.dml.chart.CTBarGrouping;
import org.docx4j.dml.chart.CTBarSer;
import org.docx4j.dml.chart.CTBoolean;
import org.docx4j.dml.chart.CTCrossBetween;
import org.docx4j.dml.chart.CTDLblPos;
import org.docx4j.dml.chart.CTDLbls;
import org.docx4j.dml.chart.CTGapAmount;
import org.docx4j.dml.chart.CTLineChart;
import org.docx4j.dml.chart.CTLineSer;
import org.docx4j.dml.chart.CTOverlap;
import org.docx4j.dml.chart.CTPlotArea;
import org.docx4j.dml.chart.CTValAx;
import org.docx4j.dml.chart.STBarDir;
import org.docx4j.dml.chart.STBarGrouping;
import org.docx4j.dml.chart.STCrossBetween;
import org.docx4j.dml.chart.STDLblPos;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;

/**
 * @author tmenzel
 * @since 23.04.2015
 */
public class XlsChartTransformer {
	/** Logger. */
	private final static Logger log = Logger
			.getLogger(XlsChartTransformer.class);

	private final SpreadsheetMLPackage root;

	private final WorkbookPart wbp;

	final static CTBoolean CT_TRUE = new CTBoolean();

	final static CTBoolean CT_FALSE = new CTBoolean();
	{
		CT_TRUE.setVal(true);
		CT_FALSE.setVal(false);
	}

	/**
	 * Instantiates a new xls chart transformer.
	 *
	 * @param xslxStream
	 *            the xslx stream - nt sure if this needs to be closed, but cant
	 *            hurt IMO
	 * @throws Docx4JException
	 *             the docx4 j exception
	 */
	public XlsChartTransformer(final InputStream xslxStream)
			throws Docx4JException {
		root = (SpreadsheetMLPackage) SpreadsheetMLPackage.load(xslxStream);
		wbp = root.getWorkbookPart();
	}

	/**
	 * Transform to bar chart.
	 *
	 * @param sheetIndex
	 *            the sheet index
	 * @param dataSeries
	 *            the config of the data series. may be null or emtpy, in this
	 *            case the defaults will be used from the chart. the index of
	 *            series will index the config, if there are too few then it is
	 *            cycled thru the configs with MOD operation
	 * @throws Exception
	 *             the exception
	 */
	public void transformToBarChart(final int sheetIndex,
			final List<DataSeriesConfig> dataSeries) throws Exception {

		final Chart chartPart = getChartOnWorksheet(sheetIndex);

		CTPlotArea plotArea = chartPart.getContents().getChart().getPlotArea();
		List<Object> valAxOrCatAxOrDateAx = plotArea.getValAxOrCatAxOrDateAx();
		for (Object object : valAxOrCatAxOrDateAx) {
			if (object instanceof CTValAx) {
				CTValAx ax = (CTValAx) object;
				CTCrossBetween val = new CTCrossBetween();
				val.setVal(STCrossBetween.BETWEEN);
				ax.setCrossBetween( val );
			}
		}
		
		
		final List<Object> charts = plotArea
				.getAreaChartOrArea3DChartOrLineChart();

		assertEquals(1, charts.size());
		assertEquals(CTLineChart.class, charts.get(0).getClass());

		final CTLineChart lineChart = (CTLineChart) charts.get(0);
		final CTBarChart barChart = createBarChartScaffolding();

		transformToBarChart(lineChart, barChart, dataSeries);

		// replace the line with the barchart
		charts.set(0, barChart);
	}

	/**
	 * @throws Docx4JException
	 */
	public void save(final OutputStream outputStream) throws Docx4JException {
		root.save(outputStream);
	}

	/**
	 * create the bar chart scaffolding and sets the bar chart unique props
	 */
	private CTBarChart createBarChartScaffolding() {
		final CTBarChart barChart = new CTBarChart();
		final CTBarDir barDir = new CTBarDir();
		barDir.setVal(STBarDir.COL);
		barChart.setBarDir(barDir);
		final CTBarGrouping grouping = new CTBarGrouping();
		grouping.setVal(STBarGrouping.STACKED);
		barChart.setGrouping(grouping);

		/**
		 * <pre>
		 * 	<c:dLbls>
		 * 		<c:showLegendKey val="0" />
		 * 		<c:showVal val="0" />
		 * 		<c:showCatName val="0" />
		 * 		<c:showSerName val="0" />
		 * 		<c:showPercent val="0" />
		 * 		<c:showBubbleSize val="0" />
		 * 	</c:dLbls>
		 * </pre>
		 */

		final CTDLbls dataLabels = new CTDLbls();
		final CTDLblPos lblPos = new CTDLblPos();
		lblPos.setVal(STDLblPos.CTR);
		dataLabels.setDLblPos(lblPos);

		dataLabels.setShowLegendKey(CT_FALSE);
		dataLabels.setShowVal(CT_TRUE);
		dataLabels.setShowCatName(CT_FALSE);
		dataLabels.setShowSerName(CT_FALSE);
		// dataLabels.setShowPercent(ctTrue);
		// dataLabels.setShowBubbleSize(ctTrue);
		dataLabels.setShowLeaderLines(CT_FALSE);
		// these are the defaults for the data series
		barChart.setDLbls(dataLabels);

		/**
		 * <pre>
		 * 	<c:gapWidth val="150" />
		 * 	<c:overlap val="100" />
		 * </pre>
		 */
		final CTGapAmount gap = new CTGapAmount();
		gap.setVal(150);
		barChart.setGapWidth(gap);
		final CTOverlap overlap = new CTOverlap();
		overlap.setVal((byte) 100);
		barChart.setOverlap(overlap);
		return barChart;
	}

	/**
	 * copies the lineChart stuff over to the bar chart + some additional
	 * settings
	 * 
	 * @param dataSers
	 */
	private void transformToBarChart(final CTLineChart lineChart,
			final CTBarChart barChart, final List<DataSeriesConfig> dataSers)
			throws IllegalAccessException, InvocationTargetException {
		// axis
		barChart.getAxId().addAll(lineChart.getAxId());
		// copy the data series to the barchar -- we dont need to mod them

		for (final CTLineSer ser : lineChart.getSer()) {
			final CTBarSer barSer = new CTBarSer();
			BeanUtilsBean2.getInstance().copyProperties(barSer, ser);

			// no need to keep these, are calced anew anyhow by EXCEL
			barSer.getCat().getNumRef().setNumCache(null);
			barSer.getVal().getNumRef().setNumCache(null);

			final DataSeriesConfig dataSer;
			if (CollectionUtils.isEmpty(dataSers)) {
				dataSer = new DataSeriesConfig(null);
			} else {
				final long colorIndex = ser.getIdx().getVal() % dataSers.size();
				dataSer = dataSers.get((int) colorIndex);
			}

			// set the color of a stack segment/series
			if (dataSer.barColor != null) {
				final CTShapeProperties shapeProps = new CTShapeProperties();
				final CTSolidColorFillProperties fill = new CTSolidColorFillProperties();
				final CTSRgbColor rgb = getCtColor(dataSer.barColor);
				fill.setSrgbClr(rgb);
				shapeProps.setSolidFill(fill);
				barSer.setSpPr(shapeProps);
			}

			// set color of label
			if (dataSer.labelColor != null) {
				// set the defaults from the chart and overwrite the others
				barSer.setDLbls(new CTDLbls());
				/*
				 * for some reason coping from default will prevent changing the
				 * line color, so we just need to set some things to false
				 * BeanUtilsBean2.getInstance().copyProperties(barSer.
				 * getDLbls(), lineChart.getDLbls());
				 */
				barSer.getDLbls().setShowLegendKey(CT_FALSE);
				barSer.getDLbls().setShowCatName(CT_FALSE);
				barSer.getDLbls().setShowSerName(CT_FALSE);

				// this CTTextBody is required but needs no content !?
				barSer.getDLbls().setTxPr(new CTTextBody());
				barSer.getDLbls().getTxPr()
						.setBodyPr(new CTTextBodyProperties());
				barSer.getDLbls().getTxPr().getP().add(new CTTextParagraph());
				final CTTextParagraph ctTextParagraph = barSer.getDLbls()
						.getTxPr().getP().get(0);
				ctTextParagraph.setPPr(new CTTextParagraphProperties());
				ctTextParagraph.getPPr().setDefRPr(
						new CTTextCharacterProperties());
				ctTextParagraph.getPPr().getDefRPr()
						.setSolidFill(new CTSolidColorFillProperties());
				ctTextParagraph.getPPr().getDefRPr().getSolidFill()
						.setSrgbClr(getCtColor(dataSer.labelColor));
			}

			barChart.getSer().add(barSer);
		}
	}

	/**
	 */
	private static CTSRgbColor getCtColor(final Color color) {
		final CTSRgbColor rgb = new CTSRgbColor();
		rgb.setVal(new byte[] { (byte) color.getRed(), (byte) color.getGreen(),
				(byte) color.getBlue() });
		return rgb;
	}

	/**
	 * @return the sole {@link Chart} on the given sheet
	 */
	public Chart getChartOnWorksheet(final int sheetIndex) throws Exception {

		final WorksheetPart wsPart = wbp.getWorksheet(sheetIndex);

		final Set<Part> foundParts = findDescendendsByType(wsPart,
				new HashSet<Part>(),
				"application/vnd.openxmlformats-officedocument.drawingml.chart+xml");

		final int size = foundParts.size();
		if (size == 0) {
			throw new IllegalArgumentException("no chart found on sheet: "
					+ getSheetRef(sheetIndex));
		} else if (size > 1) {
			throw new IllegalArgumentException("more than one chart on sheet: "
					+ getSheetRef(sheetIndex));
		}

		final Part chartPart = foundParts.iterator().next();

		return (Chart) chartPart;
	}

	/**
	 * @param sheetIndex
	 * @return
	 * @throws Docx4JException
	 */
	private String getSheetRef(final int sheetIndex) throws Docx4JException {
		return format("index=%d name='%s'", sheetIndex, wbp.getContents()
				.getSheets().getSheet().get(sheetIndex).getName());
	}

	/**
	 * Find descendends by type.
	 *
	 * @param rootPart
	 *            either part itself or its {@link RelationshipsPart}
	 * @param foundParts
	 *            set to collect the matching Parts, in case these are ref'ed
	 *            from >1 sources it is a set
	 * @param type
	 *            the type to be matched
	 * @return same as the param foundParts
	 */
	public static Set<Part> findDescendendsByType(final Part rootPart,
			final Set<Part> foundParts, final String type) {

		final RelationshipsPart rp = rootPart instanceof RelationshipsPart ? (RelationshipsPart) rootPart
				: rootPart.getRelationshipsPart(false);
		if (rp == null) {
			return foundParts;
		}

		for (final Relationship r : rp.getRelationships().getRelationship()) {

			log.info("\nFor Relationship Id=" + r.getId() + " Source is "
					+ rp.getSourceP().getPartName() + ", Target is "
					+ r.getTarget() + " type " + r.getType() + "\n");

			if (StringUtils.equals("External", r.getTargetMode())) {
				// shouldnt happen in our case
				final String msg = "external resource " + r.getTarget()
						+ " of type " + r.getType();
				log.warn(msg);
				continue;
			}

			final Part part = rp.getPart(r);

			if (type.equals(part.getContentType())) {
				foundParts.add(part);
			}

			// if (handled.get(part) != null) {
			// sb.append(" [additional reference] ");
			// continue;
			// }
			// handled.put(part, part);
			if (part.getRelationshipsPart(false) == null) {
				// sb.append(".. no rels" );
			} else {
				findDescendendsByType(part, foundParts, type);
			}

		}
		return foundParts;
	}
}
