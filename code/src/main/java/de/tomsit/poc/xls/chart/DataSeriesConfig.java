/**
 * 
 */
package de.tomsit.poc.xls.chart;

import java.awt.Color;

/**
 * makes some configs for the data series available
 * @author tmenzel
 * @since 25.04.2015
 */
public class DataSeriesConfig {

	/** The bar color. */
	Color barColor;

	/**
	 * The color for the label in the bar. if this is null then the chart
	 * default is taken.
	 */
	Color labelColor;

	/**
	 * @param barColor
	 * @param labelColor
	 */
	public DataSeriesConfig(final Color barColor, final Color labelColor) {
		super();
		this.barColor = barColor;
		this.labelColor = labelColor;
	}

	/**
	 * @param barColor
	 * @param labelColor
	 */
	public DataSeriesConfig(final Color barColor) {
		super();
		this.barColor = barColor;
		this.labelColor = labelColor;
	}

}
